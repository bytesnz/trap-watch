#define ESP32_CAM_DEV 1
#if defined(ESP32_CAM_DEV)

/* Camera pin definitions */
#define CAMERA_PWDN_PIN     32
#define CAMERA_RESET_PIN    -1
#define CAMERA_XCLK_PIN      0
#define CAMERA_SIOD_PIN     26
#define CAMERA_SIOC_PIN     27

#define CAMERA_D7_PIN       35
#define CAMERA_D6_PIN       34
#define CAMERA_D5_PIN       39
#define CAMERA_D4_PIN       36
#define CAMERA_D3_PIN       21
#define CAMERA_D2_PIN       19
#define CAMERA_D1_PIN       18
#define CAMERA_D0_PIN        5
#define CAMERA_VSYNC_PIN    25
#define CAMERA_HREF_PIN     23
#define CAMERA_PCLK_PIN     22

/* PIR sensor pin definitions */
/* NB: GPIO12 is a strapping pin */
#define PIR_SENSOR_PIN 13

/* SD Card pin definitions */
#define SD_CARD_SDMMC     1
/* Number of data lines to SD/MMC card (max 4 for SD) */
#define SD_CARD_MMC_WIDTH 1
#define SD_CARD_CLK_PIN   14
#define SD_CARD_CMD_PIN   15
#define SD_CARD_D0_PIN    2
//#define SD_CARD_D1_PIN    4 // Also the flash! Yay
//#define SD_CARD_D2_PIN    12
//#define SD_CARD_D3_PIN    13 // The PIR sensor
#define SD_CARD_D1_PIN    -1
#define SD_CARD_D2_PIN    -1
#define SD_CARD_D3_PIN    -1

#elif defined(TRAP_WATCH_V1)

#endif
