#include <stdio.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>
#include "io.h"
#include "esp_log.h"
#include "sd.h"
#include "image_capture.h"
#include "esp_sleep.h"

#include "esp_vfs_fat.h"
#include "sdmmc_cmd.h"
#include "driver/sdmmc_host.h"

// TODO Change to config?
static const char *IMAGE_FOLDER = MOUNT_POINT"/DCIM/trap-watch";

enum wait_codes {
  WAIT_FOR_NOBODY = 0,
  WAIT_FOR_PIR_SENSOR = 0x1
};

#define MAX_PIR_NO_DATE_TRIGGERS 10000

/** Whether the clock has been set or not */
RTC_DATA_ATTR bool clock_set = false;
/** Current interrupt wait state */
RTC_DATA_ATTR uint8_t wait_state = WAIT_FOR_NOBODY;
/** PIR trigger number for the day */
// Set to 0 so first call to save_photo(true), trigger number will be 1
RTC_DATA_ATTR unsigned int pir_trigger_number = 0;
/** PIR capture number when the date is not set */
RTC_DATA_ATTR unsigned int pir_capture_number = 1;
/** Last PIR trigger date. Used to test if trigger number needs to be reset */
RTC_DATA_ATTR char date[8];

/** Logging tag */
static const char *TAG = "trap-watch";

static void save_photo(int8_t new_trigger);
static esp_err_t check_image_folder(void);


void app_main(void) {
  ESP_LOGD(TAG, "Running main app");
  const uint64_t ext_wakeup_pin_1_mask = 1ULL << PIR_SENSOR_PIN;
  uint64_t wakeup_bitmask = 0;
  esp_err_t status;
  time_t now;
  struct tm current_time;

  switch (esp_sleep_get_wakeup_cause()) {
    case ESP_SLEEP_WAKEUP_EXT0:
      break;
    case ESP_SLEEP_WAKEUP_EXT1:
      wakeup_bitmask = esp_sleep_get_ext1_wakeup_status();
      ESP_LOGD(TAG, "Woke up from EXT1 interrupt %llX", wakeup_bitmask);
      save_photo(1);
      wait_state = WAIT_FOR_PIR_SENSOR;
      break;
    case ESP_SLEEP_WAKEUP_TIMER:
      ESP_LOGD(TAG, "Woke up by timer");
      // TODO Add config for continue to take photos if PIR high
      if (wait_state == WAIT_FOR_PIR_SENSOR) {
        if (gpio_get_level(PIR_SENSOR_PIN) == 1) {
          save_photo(0);
        }
      }
      break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD:
    case ESP_SLEEP_WAKEUP_ULP:
    case ESP_SLEEP_WAKEUP_GPIO:
    case ESP_SLEEP_WAKEUP_UART:
    case ESP_SLEEP_WAKEUP_WIFI:
    case ESP_SLEEP_WAKEUP_COCPU:
    case ESP_SLEEP_WAKEUP_COCPU_TRAP_TRIG:
    case ESP_SLEEP_WAKEUP_BT:
      break;
    case ESP_SLEEP_WAKEUP_UNDEFINED:
    default:
      ESP_LOGI(TAG, "Starting trap-watch"); // TODO v%s (IDF %s)", PROJECT_VER, IDF_VER);
      status = check_image_folder();
      if (status != ESP_OK) {
        ESP_LOGE(TAG, "Exiting");
        return;
      }

      // Set date string for tracking day's trigger number
      time(&now);
      localtime_r(&now, &current_time);
      strftime(date, 8, "%Y%m%d", &current_time);
  }

  ESP_LOGD(TAG, "Wait state current %d", wait_state);

  if (wait_state == WAIT_FOR_PIR_SENSOR) {
    if (gpio_get_level(PIR_SENSOR_PIN) == 1) {
      ESP_LOGD(TAG, "PIR sensor pin high. Waiting for it to go low");
      // TODO Change to config value
      esp_sleep_enable_timer_wakeup(1000 * 3000);
    } else {
      ESP_LOGD(TAG, "PIR sensor pin now low.");
      wait_state = WAIT_FOR_NOBODY;
    }
  }

  if (wait_state != WAIT_FOR_PIR_SENSOR) {
    ESP_LOGI(TAG, "Waiting for PIR trigger");
    ESP_LOGD(TAG, "Enabling EXT1 wakeup on pin %d", PIR_SENSOR_PIN);
    esp_sleep_enable_ext1_wakeup(ext_wakeup_pin_1_mask, ESP_EXT1_WAKEUP_ANY_HIGH);
  }

  ESP_LOGD(TAG, "Going to sleep");
  esp_deep_sleep_start();
}

/**
 * Capture and save a photo to the image folder
 *
 * @param new_trigger 1 for new trigger (trigger number will be incremented
 *   before saving photi), 0 for not new trigger, -1 for not related to trigger
 *
 */
static void save_photo(int8_t new_trigger) {
  sd_error_t *sd_error = NULL;

  capture_result_t *capture_result = NULL;

  char* filename = NULL;
  char filename_format[28];
  char date_now[8];
  const size_t filename_length = strlen(IMAGE_FOLDER) + 28; // /yyyymmdd nnnnnnn HHMMSS.jpg\0
  time_t now;
  struct tm current_time;

  sd_error = sd_init();

  if (sd_error != NULL) {
    ESP_LOGE(TAG, "Got an error initialising SD card 0x%X: %s", sd_error->error_code, sd_error->error);

    return;
  }

  esp_err_t error_code;
  ESP_LOGI(TAG, "Intialising the camera");
  error_code = camera_init();

  if (error_code != ESP_OK) {
    ESP_LOGE(TAG, "Error intialising camera");
    return;
  }

  ESP_LOGI(TAG, "Taking a photo");

  filename = malloc(filename_length);
  strcpy(filename, IMAGE_FOLDER);
  time(&now);
  localtime_r(&now, &current_time);
  // Check still the same date
  strftime(date_now, 8, "%Y%m%d", &current_time);
  if (clock_set && strcmp(date_now, date) != 0) {
    pir_trigger_number = 1;
    strcpy(date, date_now);
  } else if (new_trigger == 1) {
    pir_trigger_number++;
    pir_capture_number = 1;
  }

  if (clock_set) {
    strftime(filename_format, 28, "%%s/%Y%m%d %%d %H%M%S.jpg", &current_time);
    sprintf(
      filename, filename_format, IMAGE_FOLDER,
      new_trigger == -1 ? 0 : pir_trigger_number
    );
  } else {
    sprintf(
      filename, "%s/no date %d %d.jpg", IMAGE_FOLDER,
      new_trigger == -1 ? 0 : pir_trigger_number,
      pir_capture_number++
    );
  }
  ESP_LOGI(TAG, "Capturing image %s", filename);
  capture_result = capture_image(filename);

  if (capture_result->error != NULL) {
    ESP_LOGE(TAG, "Got an error taking image 0x%X: %s", capture_result->error_code, capture_result->error);
  }

  free(filename);
  sd_error = sd_deinit();

  if (sd_error != NULL) {
    ESP_LOGE(TAG, "Got an error deinitialising SD card 0x%X: %s", sd_error->error_code, sd_error->error);

    return;
  }
}

/**
 * Check that the image folder path exists and if it doesn't, create it
 *
 * @returns ESP_OK if path exists or was create successfully, ESP_FAIL
 *   otherwise
 */
static esp_err_t check_image_folder(void) {
  struct stat buf;
  char* folder;
  sd_error_t *sd_error = NULL;
  folder = (char*)malloc(strlen(IMAGE_FOLDER) + 1);
  ESP_LOGD(TAG, "Check image folder exists");
  unsigned int i = strlen(MOUNT_POINT) + 1;
  unsigned int len = strlen(IMAGE_FOLDER);
  char* filename = NULL;
  const size_t filename_length = strlen(IMAGE_FOLDER) + 28; // /yyyymmdd nnnnnnn HHMMSS.jpg\0
  bool created_folder = false;

  sd_error = sd_init();

  if (sd_error != NULL) {
    ESP_LOGE(TAG, "Got an error initialising SD card 0x%X: %s", sd_error->error_code, sd_error->error);

    return ESP_FAIL;
  }

  while (i < len) {
    while(IMAGE_FOLDER[i] != '/' && IMAGE_FOLDER[i] != '\0' && i < len) {
      i++;
    }

    strlcpy(folder, IMAGE_FOLDER, i + 1);
    // Check folder exists
    ESP_LOGD(TAG, "Checking folder %s", folder);
    if (stat(folder, &buf)) {
      if (errno == ENOENT) {
        ESP_LOGI(TAG, "Making folder %s", folder);
        if (mkdir(folder, 0750)) {
          ESP_LOGE(TAG, "Error creating creating folder %s %s", folder, strerror(errno));
          return ESP_FAIL;
        }
        created_folder = true;
      } else {
        ESP_LOGE(TAG, "Error checking folder %s %s", folder, strerror(errno));
        return ESP_FAIL;
      }
    }

    // Check the folder is a folder
    if (!S_ISDIR(buf.st_mode)) {
      ESP_LOGE(TAG, "Folder %s exists, but is not a directory", folder);
      return ESP_FAIL;
    }

    i++;
  }

  if (!created_folder) {
    filename = malloc(filename_length);
    // Find last used no date trigger number
    for (i = 1; i < MAX_PIR_NO_DATE_TRIGGERS; i++) {
      sprintf(filename, "%s/no date %d 1.jpg", IMAGE_FOLDER, i);
      if (stat(filename, &buf)) {
        if (errno == ENOENT) {
          pir_trigger_number = i-1;
          free(filename);
          ESP_LOGD(TAG, "Using next free trigger number %d for no date images", pir_trigger_number);
          break;
        }

        ESP_LOGE(TAG, "Error finding free trigger number %s", strerror(errno));
      }
    }
    if (i == MAX_PIR_NO_DATE_TRIGGERS) {
      free(filename);
      ESP_LOGE(TAG, "Could not find a free trigger number, old will be overwritten");
    }
  }

  sd_error = sd_deinit();

  if (sd_error != NULL) {
    ESP_LOGE(TAG, "Got an error deinitialising SD card 0x%X: %s", sd_error->error_code, sd_error->error);

    return ESP_FAIL;
  }

  return ESP_OK;
}
