#include "esp_err.h"

typedef enum {
  CAMERA_NOT_INITIALISED
} camera_error_code_t;

typedef struct {
  camera_error_code_t error_code;
  const char *filename;
  const char *error;
} capture_result_t;


/**
 * Initialise the camera using the current camera settings
 */
esp_err_t camera_init(void);

/**
 * Capture an image and save it to the SD card
 *
 * @return {camera_result_t} The camera capture result, which must be frees
 *   once finished with
 */
capture_result_t *capture_image(char *filename);
