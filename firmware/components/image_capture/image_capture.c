#include <stdio.h>
#include "esp_camera.h"
#include "esp_err.h"
#include "esp_log.h"
#include "image_capture.h"
#include "sd.h"
#include "io.h"

static const char *TAG = "camera";

esp_err_t camera_init(void) {
  camera_config_t camera_config = {
    .pin_pwdn = CAMERA_PWDN_PIN,
    .pin_reset = CAMERA_RESET_PIN,
    .pin_xclk = CAMERA_XCLK_PIN,
    .pin_sscb_sda = CAMERA_SIOD_PIN,
    .pin_sscb_scl = CAMERA_SIOC_PIN,

    .pin_d7 = CAMERA_D7_PIN,
    .pin_d6 = CAMERA_D6_PIN,
    .pin_d5 = CAMERA_D5_PIN,
    .pin_d4 = CAMERA_D4_PIN,
    .pin_d3 = CAMERA_D3_PIN,
    .pin_d2 = CAMERA_D2_PIN,
    .pin_d1 = CAMERA_D1_PIN,
    .pin_d0 = CAMERA_D0_PIN,
    .pin_vsync = CAMERA_VSYNC_PIN,
    .pin_href = CAMERA_HREF_PIN,
    .pin_pclk = CAMERA_PCLK_PIN,

    //XCLK 20MHz or 10MHz for OV2640 double FPS (Experimental)
    .xclk_freq_hz = 20000000,
    .ledc_timer = LEDC_TIMER_0,
    .ledc_channel = LEDC_CHANNEL_0,

    .pixel_format = PIXFORMAT_JPEG, //YUV422,GRAYSCALE,RGB565,JPEG
    .frame_size = FRAMESIZE_UXGA,    //QQVGA-UXGA Do not use sizes above QVGA when not JPEG

    .jpeg_quality = 12, //0-63 lower number means higher quality
    .fb_count = 1,       //if more than one, i2s runs in continuous mode. Use only with JPEG
    .grab_mode = CAMERA_GRAB_WHEN_EMPTY,
  };

  //initialize the camera
  esp_err_t err = esp_camera_init(&camera_config);
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "Camera Init Failed");
    return err;
  }

  return ESP_OK;
}

capture_result_t *capture_image(char *filename) {
  // Will return NULL if camera not initialised
  camera_fb_t *fb = esp_camera_fb_get();

  ESP_LOGI(TAG, "Got framebuffer pointer, picture size %zu bytes", fb->len);

  capture_result_t *result = malloc(sizeof(capture_result_t));
  result->filename = filename;

  if (fb == NULL) {
    result->error_code = CAMERA_NOT_INITIALISED;
    result->error = "Camera not initialised";

    return result;
  }

  ESP_LOGI(TAG, "Writing image file");

  sd_error_t *res = write_file(filename, fb->buf, fb->len);

  ESP_LOGI(TAG, "Done writing");

  if (res != NULL) {
    result->error_code = res->error_code;
    result->error = res->error;
    ESP_LOGE(TAG, "Error saving image 0x%X: %s", result->error_code, result->error);

    free(res);

    return result;
  }

  result->error_code = 0;
  result->error = NULL;
  result->filename = filename;

  return result;
}
