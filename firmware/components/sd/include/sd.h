#include "esp_err.h"

/** The virtual mount point for the sd card */
#define MOUNT_POINT "/sdcard"

/** SD card operation error struct */
typedef struct {
  esp_err_t error_code;
  const char *error;
} sd_error_t;

/**
 * Initialise the SD card and mount the vfs filesystem
 *
 * @returns {sd_error_t*|NULL} NULL if the write was successful, or an
 *  sd_error_t* if the write was not successful.
 */
sd_error_t *sd_init(void);


/**
 * Deinitialise the SD card and umount the VFS filesystem
 *
 * @returns {sd_error_t*|NULL} NULL if the write was successful, or an
 *  sd_error_t* if the write was not successful.
 */
sd_error_t *sd_deinit(void);

/**
 * Write the given file to the SD card
 *
 * @param filename Filename of the file to write. Should be within the SD card
 *  mount directory (@see MOUNT_POINT)
 * @param buf Buffer containing the file contents
 * @param buf_len Length of the file contents
 *
 * @returns {sd_error_t*|NULL} NULL if the write was successful, or an
 *  sd_error_t* if the write was not successful.
 */
sd_error_t *write_file(const char *filename, const unsigned char *buf, ssize_t buf_len);

