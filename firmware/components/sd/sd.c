#include <stdio.h>
#include "esp_vfs_fat.h"
#include "esp_err.h"
#include "driver/sdmmc_host.h"
#include "driver/sdspi_host.h"
#include "sdmmc_cmd.h"
#include "sd.h"
#include "io.h"

#define TAG "SD Card"

void func(void)
{

}

void *card = NULL;

sd_error_t *sd_init(void) {
#ifdef SD_CARD_SDMMC
  ESP_LOGI(TAG, "Initialising SDMMC");

  esp_err_t ret;

  // Options for mounting the filesystem.
  // If format_if_mount_failed is set to true, SD card will be partitioned and
  // formatted in case when mounting fails.
  esp_vfs_fat_sdmmc_mount_config_t mount_config = {
    // TODO Could add config?
    .format_if_mount_failed = false,
    .max_files = 5,
    .allocation_unit_size = 16 * 1024
  };

  // Use settings defined above to initialize SD card and mount FAT filesystem.
  // Note: esp_vfs_fat_sdmmc/sdspi_mount is all-in-one convenience functions.
  // Please check its source code and implement error recovery when developing
  // production applications.

  sdmmc_host_t host = SDMMC_HOST_DEFAULT();

  // This initializes the slot without card detect (CD) and write protect (WP) signals.
  // Modify slot_config.gpio_cd and slot_config.gpio_wp if your board has these signals.
  sdmmc_slot_config_t slot_config = SDMMC_SLOT_CONFIG_DEFAULT();

  // Set the number of data lines to the SD/MMC card
  slot_config.width = SD_CARD_MMC_WIDTH;

  // On chips where the GPIOs used for SD card can be configured, set them in
  // the slot_config structure:
#ifdef SOC_SDMMC_USE_GPIO_MATRIX
  slot_config.clk = SD_CARD_CLK_PIN;
  slot_config.cmd = SD_CARD_CMD_PIN;
  slot_config.d0 = SD_CARD_D0_PIN;
  slot_config.d1 = SD_CARD_D1_PIN;
  slot_config.d2 = SD_CARD_D2_PIN;
  slot_config.d3 = SD_CARD_D3_PIN;
#endif

  // Enable internal pullups on enabled pins. The internal pullups
  // are insufficient however, please make sure 10k external pullups are
  // connected on the bus. This is for debug / example purpose only.
  slot_config.flags |= SDMMC_SLOT_FLAG_INTERNAL_PULLUP;

  ret = esp_vfs_fat_sdmmc_mount(MOUNT_POINT, &host, &slot_config, &mount_config, (sdmmc_card_t **)&card);

  if (ret != ESP_OK) {
    sd_error_t *res = malloc(sizeof(sd_error_t));
    res->error_code = ret;
    if (ret == ESP_FAIL) {
      res->error = "Failed to mount SD card filesystem";
    } else {
      res->error = esp_err_to_name(ret);
    }
    return res;
  }

  ESP_LOGI(TAG, "Filesystem mounted");

  // Card has been initialized, print its properties
  sdmmc_card_print_info(stdout, card);
#endif

  return NULL;
}

sd_error_t *sd_deinit(void) {
  esp_vfs_fat_sdcard_unmount(MOUNT_POINT, card);
  ESP_LOGI(TAG, "Card unmounted");

  return NULL;
}

sd_error_t *write_file(const char *filename, const unsigned char *buf, ssize_t buf_len) {

  FILE *f = fopen(filename, "w");

  if (f == NULL) {
    sd_error_t *res = malloc(sizeof(sd_error_t));
    res->error_code = ESP_FAIL;
    res->error = "Could not open file";
    return res;
  }

  fwrite(buf, buf_len, sizeof(char), f);

  fclose(f);

  return NULL;
}
