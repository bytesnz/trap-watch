trap-watch Board
----------------

Custom PCB for trap-watch using the ESP32-S3-WROOM-1 module with connectivity
for:

- A camera serial interface (CSI) camera
- A microSD card
- A micro USB for data transfer
- LED flash (up to 180mA)
- An SPI module like GPS
- An I2C module like a temperature and humdity sensor
- At least 3 trap triggers
- A passive infrared (PIR) sensor

It has also been designed to be used as a litter picker camera.

![trap-watch board top](output/trap-watch_top.png)

![trap-watch board bottom](output/trap-watch_bottom.png)
