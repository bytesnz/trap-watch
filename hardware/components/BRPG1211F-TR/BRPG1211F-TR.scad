height = 1;
flatHeight = 0.6;
curveRadius = 0.3;
width = 3;
flatWidth = 2.4;
depth = 1;
domeDepth = 1;
domeWidth = 2;
emitterHeightCentre = 0.4;
emitterWidthCentre = 0.7;
contactHeight = 0.4;
contactWidth = 0.5;
contactThickness = 0.01;
touchThickness = 0.001;
cylinderFaces = 70;
cathodeWidth = (height - flatHeight) / 2;

// Body
difference() {
    color("white")
    cube([width, depth, height]);
    color("silver")
    rotate([90, 0, 0])
    translate([0, 0, -depth-0.001]) cylinder(r=curveRadius, h=depth + 0.002, $fn = cylinderFaces);
    color("silver") rotate([90, 0, 0]) translate([0, height, -depth-0.001]) cylinder(r=curveRadius, h=depth + 0.002, $fn = cylinderFaces);
    color("silver") rotate([90, 0, 0]) translate([width, 0, -depth-0.001]) cylinder(r=curveRadius, h=depth + 0.002, $fn = cylinderFaces);
    color("silver") rotate([90, 0, 0]) translate([width, height, -depth-0.001]) cylinder(r=curveRadius, h=depth + 0.002, $fn = cylinderFaces);
}

// Lens

// Body
difference() {
    color("white", 0.6)
    translate([width / 2, depth - 0.001, 0])
    cylinder(r=1, h=height, $fn = cylinderFaces);
    color("white")
    cube([width, depth, height]);
}

//Anode
// Back
difference() {
    color("silver")
    translate([width - contactWidth, - 0.01 + 0.001, 0])
    cube([ contactWidth, 0.01, contactHeight ]);
    color("silver")
    rotate([90, 0, 0])
    translate([width, 0,  -0.002]) cylinder(r=curveRadius, h=0.01 + 0.002, $fn = cylinderFaces);
}

difference() {
    color("silver")
    translate([width - contactWidth, - 0.01 + 0.001, height - contactHeight])
    cube([ contactWidth, 0.01, contactHeight ]);
    color("silver")
    rotate([90, 0, 0])
    translate([width, height,  -0.002]) cylinder(r=curveRadius, h=0.01 + 0.002, $fn = cylinderFaces);
}

// Front
difference() {
    color("silver")
    translate([width - contactWidth, depth - 0.001, 0])
    cube([ contactWidth, 0.01, height ]);
    color("silver")
    rotate([90, 0, 0])
    translate([width, 0,  -depth - contactThickness]) cylinder(r=curveRadius, h=0.01 + 0.002, $fn = cylinderFaces);
    color("silver")
    rotate([90, 0, 0])
    translate([width, height,  -depth - contactThickness]) cylinder(r=curveRadius, h=0.01 + 0.002, $fn = cylinderFaces);
}

//Yellow (Outer) Cathode
// Back
difference() {
    color("silver")
    translate([0, - 0.01 + 0.001, 0])
    cube([ contactWidth, 0.01, contactHeight ]);
    color("silver")
    rotate([90, 0, 0])
    translate([0, 0,  -0.002]) cylinder(r=curveRadius, h=0.01 + 0.002, $fn = cylinderFaces);
}


// Front
/*XXX difference() {
    color("silver")
    translate([0, depth - 0.001, 0])
    cube([ contactWidth, 0.01, height ]);
    color("silver")
    rotate([90, 0, 0])
    translate([0, 0,  -depth - contactThickness]) cylinder(r=curveRadius, h=0.01 + 0.002, $fn = cylinderFaces);
    color("silver")
    rotate([90, 0, 0])
    translate([0, height,  -depth - contactThickness]) cylinder(r=curveRadius, h=0.01 + 0.002, $fn = cylinderFaces);
}*/
difference() {
    color("silver")
    translate([0, depth - 0.001, 0])
    cube([ contactWidth, 0.01, contactHeight ]);
    color("silver")
    rotate([90, 0, 0])
    translate([0, 0,  -depth -contactThickness]) cylinder(r=curveRadius, h=0.01 + 0.002, $fn = cylinderFaces);
}

// Red (Inner) Cathode
// Front
difference() {
    color("silver")
    translate([0, depth - 0.001, height - contactHeight])
    cube([ contactWidth, 0.01, contactHeight ]);
    color("silver")
    rotate([90, 0, 0])
    translate([0, height,  -depth -contactThickness]) cylinder(r=curveRadius, h=0.01 + 0.002, $fn = cylinderFaces);
}

// Back
// Corner
difference() {
    color("silver")
    translate([0, - contactThickness + 0.001, height - contactHeight])
    cube([ contactWidth, 0.01, contactHeight ]);
    color("silver")
    rotate([90, 0, 0])
    translate([0, height,  -
    0.002]) cylinder(r=curveRadius, h=0.01 + 0.002, $fn = cylinderFaces);
}

// Top
color("silver")
translate([ contactWidth, -contactThickness + 0.001, height - cathodeWidth])
cube([ width / 2 - contactWidth, contactThickness, cathodeWidth]);

// Down
color("silver")
translate([ (width -cathodeWidth) / 2, -contactThickness + 0.001, height / 2])
cube([ cathodeWidth, contactThickness, height / 2]);

// Bottom
color("silver")
translate([width / 2, 0.001, 0])
difference() {
scale([1,1,height/0.7])
rotate([90, 0, 0])
cylinder(d=0.8, h=contactThickness, $fn=cylinderFaces);
translate([-(0.8+0.002)/2, -contactThickness - 0.001, -1])
cube([0.8 + 0.002, contactThickness + 0.002, 1]);
}