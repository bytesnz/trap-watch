# trap-watch

Opensource ESP32-S2-based trap monitor

[![developtment time](https://bytes.nz/b/trap-watch/custom?color=yellow&name=development+time&value=~2+hours)](https://gitlab.com/bytesnz/trap-watch/blob/main/.tickings)
[![support development](https://bytes.nz/b/trap-watch/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/ArticuTec/)
[![contributor covenant](https://bytes.nz/b/trap-watch/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](https://gitlab.com/bytesnz/trap-watch/blob/main/code-of-conduct.md)

# Currently in Development

The trap-watch monitor is currently under development. Watch the repository
for updates or check out the
[list of issues](https://gitlab.com/bytesnz/trap-watch/-/issues) and the
[development board](https://gitlab.com/bytesnz/trap-watch/-/boards)
for current development progress.

# Introduction

This project was started with the desire to be able to monitor
[pest][doc-pests] traps to figure out what was tampering with the traps and
to be able to record the specific time a pest is caught. The ability to be
notified when something was caught was also high on the list of ideas for the
project.

With a majority of trapping happening on shoestring budgets, a super cheap way
of achieving this was also high on the priority list. After a little bit of
research into microprocessors that could interface with cameras, the ESP32
line of processors was chosen, and development started.

This repository contains the result of the development - a ~$20 trap monitor
with integrated camera.

# Features

- Micro-USB port and PCB through holes for power (5V<sub>DC</sub>)
- Passive infrared sensor (PIR) for triggering camera when animal is within
  line of sight [**TODO**](https://gitlab.com/bytesnz/trap-watch/-/issues/10)
- Swappable camera module depending on trap monitor position
- Connectors for:
  - touch sensor/button for activating WiFi
  - trap trigger switches
  - integrating I2C sensors, such as temperature and humidity sensors
  - infrared LEDs for night vision
- Integrated web server and WiFi access point for viewing recorded footage and
  logs
- Automatic time syncronisation when connected to the Internet
- Automatic over-the-air (OTA) updates when connected to the Internet
- Submission of status and trap triggers

# Getting One

Watch this space...

# Development

Development of the hardware has been carried out using [KiCAD][] and
development of the firmware has been carried out using the
[Espressif IoT Development Framework][esp-idf].

[doc-pests]: https://dxcprod.doc.govt.nz/nature/pests-and-threats/animal-pests/
[kicad]: https://www.kicad.org/
[esp-idf]: https://www.espressif.com/en/products/sdks/esp-idf
[esp32-s2]: https://www.espressif.com/en/products/socs/esp32-s2
